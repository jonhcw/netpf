﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Nimsoft.NimBUS.Probe;
using NetPf.ProfileEngine;
using Nimsoft.NimBUS.Messaging;

namespace NetPf.NetPfProbe
{
    class NetPfProbe : NimbusProbe
    {
        public ProfileEngine.ProfileEngine Engine { get; set; }
    }


    // This is Use Case Example!
    class SecondProbe : NetPfProbe
    {
        public void jee()
        {
            Engine = new ProfileEngine.ProfileEngine(5);
        }

        public override void StopCallback(NimbusSession session)
        {
            this.Engine.Stop();
            while (this.Engine.IsRunning) Thread.Sleep(200);
            base.StopCallback(session);
        }
    }
}
