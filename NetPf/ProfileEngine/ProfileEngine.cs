﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace NetPf.ProfileEngine
{
    /// <summary>
    /// ProfileEngine handles profiles and threading
    /// </summary>
    public class ProfileEngine
    {
        private List<Profile> Profiles { get; set; } = new List<Profile>();
        public bool IsRunning { get; private set; } = false;
        private TaskFactory<ProfileResult> _ProfileFactory;
        private Task[] ProfileEngineInternalTasks = new Task[2];
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        public CancellationToken CancellationToken { get; }

        /// <summary>
        /// Create new instance of ProfileEngine
        /// </summary>
        /// <param name="ConcurrencyLimit">Sets how many tasks can run in parallel</param>
        public ProfileEngine(int ConcurrencyLimit)
        {
            _ProfileFactory = new TaskFactory<ProfileResult>(new ProfileScheduler(ConcurrencyLimit));
            this.CancellationToken = _cancellationTokenSource.Token;

        }

        /// <summary>
        /// Add a profile to be scheduled by the ProfileEngine
        /// </summary>
        /// <param name="Profile">Instance of the Profile class</param>
        public void Add(Profile Profile)
        {
            try {
                this.Profiles.Add(Profile);
            } catch (Exception x)
            {
                throw x;
            }
        }

        /// <summary>
        /// Add profilesto be scheduled by the ProfileEngine
        /// </summary>
        /// <param name="Profiles">IEnumerable collection of Profile class</param>
        public void AddRange(IEnumerable<Profile> Profiles)
        {
            try
            {
                this.Profiles.AddRange(Profiles);
            }
            catch (Exception x)
            {
                throw x;
            }
        }

        /// <summary>
        /// Start the ProfileEngine. This will make the ProfileEngine run all profiles added to it. This should only be called once in a program.
        /// </summary>
        public void Run()
        {
            IsRunning = true;

            // Start task manager
            ProfileEngineInternalTasks[0] = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("ProfileRunner starting..");
                while (this.CancellationToken.IsCancellationRequested == false)
                {
                    int epochNow = Helpers.Time.GetEpochNow();
                    var dueProfiles = from rp in Profiles where rp.NextRun <= epochNow select rp;
                    //Console.WriteLine("Runner: {0} profiles are due", dueProfiles.Count());
                    lock (Profiles)
                    {
                        foreach (Profile p in dueProfiles)
                        {
                            p.Task = _ProfileFactory.StartNew(p.Action, this.CancellationToken);
                            if (p.NextRun == 0) p.NextRun = epochNow + p.Interval;
                            else p.NextRun += p.Interval;
                        }
                    }

                    Thread.Sleep(200);
                }

                Console.WriteLine("----- ProfileRunner has been cancelled!");
                var RunningProfiles = from rp in Profiles
                                      where rp.Task != null && rp.Task.Status != TaskStatus.RanToCompletion && rp.Task.Status != TaskStatus.Canceled && rp.Task.Status != TaskStatus.Faulted
                                      select rp.Task;
                Task[] RunningTasks = RunningProfiles.ToArray<Task>();
                try
                {
                    if (RunningProfiles.Count() > 0) Task.WaitAll(RunningTasks);
                }
                catch (AggregateException)
                {
                    Console.WriteLine("Task canceled..");
                }
                finally
                {
                    Console.WriteLine("ProfileRunner: All child tasks have finished!");
                }

                IsRunning = false;
            }, this.CancellationToken);
            
            // Start processing results
            ProfileEngineInternalTasks[1] = Task.Factory.StartNew(() =>
            {
                int totalCount = 0;
                Console.WriteLine("ProfileResultListener starting..");
                while (this.CancellationToken.IsCancellationRequested == false)
                {
                    Task<ProfileResult>[] _tasksToWait;

                    lock (Profiles)
                    {
                        var FinishedProfiles = from rp in Profiles
                                               where rp.Task != null && (rp.Task.Status == TaskStatus.RanToCompletion || rp.Task.Status == TaskStatus.Faulted)
                                               select rp.Task;
                        _tasksToWait = FinishedProfiles.ToArray<Task<ProfileResult>>();
                    }

                    if (_tasksToWait.Length > 0)
                    {
                        int i = -1;
                        i = Task.WaitAny(_tasksToWait);
                        if (i > -1)
                        {
                            totalCount++;
                            foreach (Profile p in Profiles)
                            {
                                if (p.Task == _tasksToWait[i])
                                {
                                    p.Task.Dispose();
                                    p.Task = null;
                                    break;
                                }
                            }
                        }

                    }
                    else
                    {
                        Thread.Sleep(200);
                    }
                }

                Console.WriteLine("ProfileResultListener is done! processed {0} results", totalCount);
            }, this.CancellationToken);

            Console.WriteLine("ProfileEngine is running");
        }

        /// <summary>
        /// Stops the ProfileEngine gracefully. Signals the ProfileEgine and all it's children (running profiles) to stop.
        /// </summary>
        public void Stop()
        {
            _cancellationTokenSource.Cancel();
            Task.WaitAll(ProfileEngineInternalTasks);
            Console.WriteLine("Stop done");
        }

    }
}
