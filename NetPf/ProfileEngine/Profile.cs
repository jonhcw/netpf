﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nimsoft.NimBUS.Messaging;

namespace NetPf.ProfileEngine
{

    /// <summary>
    /// Profile contains all the information needed by the profile Engine to run a monitoring function.
    /// </summary>
    public class Profile
    {
        /// <summary>
        /// Name of the profile, preferrably unique
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Determines the interval the profile is run in seconds
        /// </summary>
        public int Interval { get; set; }

        /// <summary>
        /// The next scheduled run
        /// </summary>
        public int NextRun { get; set; } = 0;

        /// <summary>
        /// Informs whether the last run didn't occur as scheduled
        /// </summary>
        public int LastRunLate { get; } = 0;

        /// <summary>
        /// The monitoring function delegate
        /// </summary>
        public Func<ProfileResult> Action { get; }

        /// <summary>
        /// Gets the task started by the ProfileEngine TaskScheduler for this profile
        /// </summary>
        public Task<ProfileResult> Task { get; set; }

        /// <summary>
        /// Instantiate new monitoring profile
        /// </summary>
        /// <param name="Name">Name of the profile, preferrably unique</param>
        /// <param name="Action">The monitoring function delegate</param>
        /// <param name="Interval">Determines the interval the profile is run in seconds</param>
        public Profile(string Name, Func<ProfileResult> Action, int Interval)
        {
            this.Name = Name;
            this.Action = Action;
            this.Interval = Interval;
        }
    }

    /// <summary>
    /// Tasks always return a ProfileResult that ProfileEngine result listener processes
    /// </summary>
    public class ProfileResult
    {
        /// <summary>
        /// List of Qosses returned by the profile task
        /// </summary>
        public List<QoS> Qosses { get; set; }

        /// <summary>
        /// List of Alarms returned by the profile task
        /// </summary>
        public List<Alarm> Alarms { get; set; }

        /// <summary>
        /// Epoch format time when the task was actually ran. Used to determine whether it was run on schedule
        /// </summary>
        public int ActualStartTime { get; set; }
    }

}
