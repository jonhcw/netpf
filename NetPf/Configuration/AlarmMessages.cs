﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nimsoft.NimBUS.Diagnostics;
using Nimsoft.NimBUS.Configuration;

namespace NetPf.Configuration
{
    public class AlarmMessages
    {
        public Dictionary<string, string> messages { get; set; } = new Dictionary<string, string>();

        public AlarmMessages(FileConfig Config)
        {
            foreach (string Key in Config.GetKeyList("messages"))
            {
                if (messages.ContainsKey(Key))
                {
                    // LOG HERE
                } else
                {
                    messages.Add(Key, Config.GetValueAsString("messages", Key));
                }
            }
        }
    }

    public class AlarmMessage
    {
        public string Message { get; set; }
        public List<string> Variables { get; set; }

        public AlarmMessage(string Message)
        {
            this.Message = Message;


        }
    }
}
