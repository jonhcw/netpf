﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Nimsoft.NimBUS.Diagnostics;
using Nimsoft.NimBUS.Configuration;
using Nimsoft.NimBUS.Messaging;

namespace NetPf.Configuration
{
    public abstract class Profile
    {
        public LogLevel LogLevel { get; set; }
        public ProfileOverrides Overrides { get; set; } = new ProfileOverrides();
        public List<Threshold> Thresholds { get; set; } = new List<Threshold>();
        public Delegate Action { get; set; }

    }

    public class ProfileOverrides
    {
        public string Origin { get; set; }
        public string Hostname { get; set; }
        public string Source { get; set; }
    }

    public abstract class Threshold
    {
    }

    public class Threshold<T> : Threshold
    {
        public T Value { get; set; }
        public string Operator { get; set; }
        public string Message { get; set; }
        public SeverityLevel Severity { get; set; }
    }


}
