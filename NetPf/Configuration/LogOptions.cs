﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nimsoft.NimBUS.Diagnostics;
using Nimsoft.NimBUS.Configuration;

namespace NetPf.Configuration
{
    public class LogOptions
    {
        public LogLevel LogLevel { get; set; }
        public int LogSize { get; set; }
        public String LogPrefix { get; set; }

        public LogOptions(FileConfig Config)
        {
            this.LogLevel = (LogLevel)Config.GetValueAsInt("setup", "loglevel", 0);
            this.LogSize = Config.GetValueAsInt("setup", "Logsize", 1024);
            this.LogPrefix =  Config.GetValueAsString("setup", "logprefix", "$loglevel");
        }
        
    }

}
