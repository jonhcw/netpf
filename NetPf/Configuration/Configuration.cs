﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetPf.Configuration
{
    class Configuration
    {
        public LogOptions LogOptions {get;set;}
        public AlarmMessages Messages { get; set; }
        public Profile Profiles { get; set; }
    }
}
