﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nimsoft.NimBUS.Probe;
using Nimsoft.NimBUS.Messaging;
using Nimsoft.NimBUS;

namespace NetPf.NetPfAlarm
{
    public class NetPfCiAlarm
    {
        private NimbusProbe Probe { get; } = null;
        public CIHandle CiHandle { get; set; } = null;
        public string MetricId { get; set; } = String.Empty;
        public string Message { get; set; } = String.Empty;
        public SeverityLevel SeverityLevel { get; set; }
        public string SuppKey { get; set; } = String.Empty;
        public string SubSystem { get; set; } = String.Empty;
        public string Source { get; set; } = String.Empty;

        /// <summary>
        ///  Initialize NetPfAlarm with local CI
        /// </summary>
        public NetPfCiAlarm(NimbusProbe Probe, string CiType, string CiName, string MetricId, string Message, SeverityLevel Severity, string SuppKey, string SubSystem, string Source)
        {
            /// Alarm with local CI
            if (Probe == null) throw new ArgumentNullException("Probe");
            if (String.IsNullOrEmpty(Message)) throw new ArgumentNullException("Message");

            this.Probe = Probe;
            this.CiHandle = Probe.ciOpenLocalDevice(CiType, CiName);
            this.Message = Message;
            this.SeverityLevel = SeverityLevel;
            this.SuppKey = SuppKey;
            this.SubSystem = SubSystem;
            this.Source = Source;
        }

        /// <summary>
        /// Initialize NetPfAlarm with remote CI
        /// </summary>
        public NetPfCiAlarm(NimbusProbe probe, string CiType, string CiName, string MetricId, string Target, string Message, SeverityLevel Severity, string SuppKey, string SubSystem, string Source)
        {
            if (Probe == null) throw new ArgumentNullException("Probe");
            if (String.IsNullOrEmpty(Message)) throw new ArgumentNullException("Message");

            this.Probe = Probe;
            this.CiHandle = probe.ciOpenRemoteDevice(CiType, CiName, Target);
            this.Message = Message;
            this.SeverityLevel = SeverityLevel;
            this.SuppKey = SuppKey;
            this.SubSystem = SubSystem;

        }

        /// <summary>
        /// Sends the alarm
        /// </summary>
        public void SendAlarm()
        {
            int ciAlarmSent = -1;
            try {
                ciAlarmSent = this.Probe.ciAlarm(this.CiHandle, this.MetricId, this.Message, (int)this.SeverityLevel, this.SuppKey, this.SubSystem, this.Source, null);
            }
            catch (Exception x)
            {
                // LOG EXCEPTION
            } finally
            {
                this.CiHandle.Dispose();
            }

            if (ciAlarmSent != 0)
            {
                PDS simpleAlarmReturn = this.Probe.PostMessage(new Alarm(this.Message, this.SeverityLevel, this.SubSystem, this.SuppKey, this.Source));
            }
        }
    }
}
